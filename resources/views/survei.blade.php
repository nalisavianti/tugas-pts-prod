@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Quissioner') }}</div>

                <div class="card-body">
                    <form method="POST" action="">
                        @csrf

                        <div class="form-group row">
                            <label for="nama" class="col-md-4 col-form-label text-md-right">{{ __('Nama') }}</label>

                            <div class="col-md-6">
                                <input id="nama" type="text" class="form-control @error('nama') is-invalid @enderror" name="nama" value="{{ old('nama') }}" required autocomplete="nama" autofocus>

                                @error('nama')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="usia" class="col-md-4 col-form-label text-md-right">{{ __('Usia') }}</label>

                            <div class="col-md-6">
                                <input id="usia" type="number" class="form-control @error('usia') is-invalid @enderror" name="usia" required autocomplete="usia">

                                @error('usia')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="jk" class="col-md-4 col-form-label text-md-right">{{ __('Jenis Kelamin') }}</label>

                            <div class="col-md-6">
                                <input id="jk" type="radio" class="@error('jk') is-invalid @enderror" name="jk" value="pria" required> Laki - laki
                                <input id="jk" type="radio" class="@error('jk') is-invalid @enderror" name="jk" value="wanita" required> Perempuan

                                @error('jk')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <table  class="table">
                                <tr>
                                    <td colspan="2"><h3>A. Potensi Tertular diluar Rumah</h3></td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" name="quest[]" value="true"/></td>
                                    <td> Saya pergi keluar rumah</td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" name="quest[]" value="true"/></td>
                                    <td>Saya menggunakan transportasi umum:online,angkot,bus,taksi,kereta api</td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" name="quest[]" value="true"/></td>
                                    <td>Saya tidak memakai masker pada saat berkumpul dengan orang lain</td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" name="quest[]" value="true"/></td>
                                    <td>Saya berjabat tangan dengan orang lain</td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" name="quest[]" value="true"/></td>
                                    <td>Saya tidak membersihkan tangan dengan handsanitizer/tissu basah sebelum pegang kemudi mobil/motor</td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" name="quest[]" value="true"/></td>
                                    <td>Saya menyentuh benda/uang yang juga disentuh oranglain</td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" name="quest[]" value="true"/></td>
                                    <td>Saya tidak menjaga jarak 1,5 meter dengan orang lain ketika:belanja,bekerja,belajar,ibadah</td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" name="quest[]" value="true"/></td>
                                    <td>Saya makan diluar rumah (warung/restaurant) </td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" name="quest[]" value="true"/></td>
                                    <td>Saya tidak minum air hangat dan cuci tangan dengan sabun setelah tiba ditujuan </td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" name="quest[]" value="true"/></td>
                                    <td>Saya berada di wilayah kelurahan tempat pasien tertular</td>
                                </tr>
                                <tr>
                                    <td colspan="2"><h3>B. Potensi Tertular didalam Rumah</h3></td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" name="quest[]" value="true"/></td>
                                    <td>Saya tidak pasang handsinitizer didepan pintu masuk,untuk kebersihan tangan sebelum pegang handle pintu masuk rumah </td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" name="quest[]" value="true"/></td>
                                    <td>Saya tidak memcuci tangan dengan sabun setelah tiba di rumah</td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" name="quest[]" value="true"/></td>
                                    <td>Saya tidak menyediakan :tissu basah/antiseptic,masker,sabun bagi keluarga di rumah </td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" name="quest[]" value="true"/></td>
                                    <td>Saya tidak segera meredam baju dan celana bekas pakai diluar rumah kedalam air panas/sabun </td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" name="quest[]" value="true"/></td>
                                    <td>Saya tidak segera mandi keramas setelah saya tiba dirumah</td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" name="quest[]" value="true"/></td>
                                    <td>Saya tidak segera mensosialisasikan checklist penilaian resiko pribadi ini kepada keluarga dirumah</td>
                                </tr>
                                <tr>
                                    <td colspan="2"><h3>C. Daya Tahan Tubuh (Imunitas)</h3></td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" name="quest[]" value="true"/></td>
                                    <td>Saya dalam sehari tidak terkena cahaya matahari min 15 menit</td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" name="quest[]" value="true"/></td>
                                    <td>Saya tidak jalan kaki/berolahraga min 30 menit setiap hari </td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" name="quest[]" value="true"/></td>
                                    <td>Saya jarang minum vit C & E, dan kurang tidur </td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" name="quest[]" value="true"/></td>
                                    <td>Usia saya diatas 60 tahun</td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" name="quest[]" value="true"/></td>
                                    <td>Saya mempunyai penyakit : Jantung/diabetes/gangguan pernafasan kronik</td>
                                </tr>
                            </table>
                        </div>

                        <div class="form-group row mb-0 float-right">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Submit') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
