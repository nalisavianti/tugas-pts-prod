@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">FORM COVID-19!</div>

                <div class="card-body">
                    <p>Selamat datang di Survey Penanganan covid-19.<br/>Kami menyiapkan beberapa pertanyaan mengenai Resiko kegiatan.</br><strong>Mohon diisi dengan cara menceklis sesuai dengan kegiatan harian anda.</strong></p>
                    <a href="{{ route('survei') }}" class="btn btn-primary">Mulai Survey!</a>
                </div>
            </div>

            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Usia</th>
                        <th scope="col">Jenis Kelamin</th>
                        <th scope="col">Resiko</th>
                    </tr>
                </thead>
                <body>
                    @php $no = 1; @endphp
                    @foreach($peserta as $data)
                        <tr>
                            <th scope="row">{{$no++}}</th>
                            <td>{{$data->nama}}</td>
                            <td>{{$data->usia}}</td>
                            <td>{{$data->jk == 'wanita' ? 'Perempuan' : 'Laki - laki'}}</td>
                            <td>
                                @if($data->skor <= 7) 
                                    <span class="btn btn-success">Rendah</span> 
                                @elseif($data->skor <= 14) 
                                    <span class="btn btn-warning">Sedang</span>
                                @else 
                                    <span class="btn btn-danger">Tinggi</span> 
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
