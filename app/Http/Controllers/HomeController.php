<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//model
use App\Peserta;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $peserta = Peserta::select('nama', 'usia', 'jk', 'skor')->orderBy('created_at', 'desc')->get();
        return view('home', compact('peserta'));
    }
}
