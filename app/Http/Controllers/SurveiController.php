<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
//model
use App\Peserta;

class SurveiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('survei');
    }

    public function submit(Request $req)
    {
        $validatedData = $req->validate([
            'nama' => ['required', 'string', 'max:100'],
            'usia' => ['required'],
            'jk' => ['required']
        ]);

        $peserta = new Peserta;
        $peserta->nama = $req->nama;
        $peserta->usia = $req->usia;
        $peserta->jk = $req->jk;
        $peserta->skor = count($req->input('quest'));
        $peserta->save();

        return redirect()->route('home');
    }
}
